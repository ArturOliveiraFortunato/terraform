# README #

Terraform Exercise

#### Requirements

- terraform (v1.1.7)
- aws CLI (v2.4.20)
#### Configuration

1. Configure your AWS access

Run `aws configure` and add your credentials for AWS

#### How to create the infrastructure

`terraform apply`

__NOTE__: Run `terraform plan` first to see the actions that will be taken

#### How to destroy the infrastructure

`terraform destroy`