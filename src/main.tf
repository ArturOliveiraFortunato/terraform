provider "aws" {
    region = "eu-west-1"
}

// Remote state
terraform {
  backend "s3" {
    bucket = "cocus-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

locals {
    vpc_cidr_block = "172.16.0.0/23"

    public_subnet_cidr = "172.16.0.0/24"
    private_subnet_cidr = "172.16.1.0/24"

    ec2_instances = {
        webserver = {
            instance_type = "t2.micro",
            ami = "ami-0069d66985b09d219"
            subnet_id = module.subnet.public_subnet_id
            associate_public_ip_address = true
            key_name = "webserver"

            security_groups = [
                module.security_group.public_sg_id
            ]

            tags = {
                Name = "webserver"
            }
            
        },
        database = {
            instance_type = "t2.micro",
            ami = "ami-0069d66985b09d219"
            subnet_id = module.subnet.private_subnet_id
            associate_public_ip_address = false
            key_name = "database"

            security_groups = [
                module.security_group.private_sg_id
            ]

            tags = {
                Name = "database"
            }
        }
    }
}

module "vpc" {
  source = "./modules/vpc"

  cidr_block = local.vpc_cidr_block
}

module "internet_gateway" {
  source = "./modules/internet_gateway"

  vpc_id = module.vpc.id
}

module "subnet" {
    source = "./modules/subnet"

    vpc_id = module.vpc.id
    public_subnet_cidr = local.public_subnet_cidr
    private_subnet_cidr = local.private_subnet_cidr
}

module "route_table" {
    source = "./modules/route_table"

    vpc_id = module.vpc.id
    ig_id = module.internet_gateway.id
    public_subnet_id = module.subnet.public_subnet_id
}

module "security_group" {
    source = "./modules/security_group"

    vpc_id = module.vpc.id
    public_subnet_cidr  = local.public_subnet_cidr
    private_subnet_cidr = local.private_subnet_cidr
}

module "ec2_instances" {
    source = "./modules/ec2_instances"

    configuration = local.ec2_instances
}