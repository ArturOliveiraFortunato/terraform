resource "aws_subnet" "public" {
    vpc_id     = var.vpc_id
    cidr_block = var.public_subnet_cidr

    tags = {
        Name = "public"
    }
}

resource "aws_subnet" "private" {
    vpc_id     = var.vpc_id
    cidr_block = var.private_subnet_cidr

    tags = {
        Name = "private"
    }
}