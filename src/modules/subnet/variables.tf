variable "vpc_id" {
    description = "Id of the VPC to place the subnets"
    type = string
}

variable "public_subnet_cidr" {
    description = "CIDR block for the public subnet"
    type = string
}

variable "private_subnet_cidr" {
    description = "CIDR block for the private subnet"
    type = string
}