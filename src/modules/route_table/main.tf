resource "aws_route_table" "public" {
	vpc_id = var.vpc_id

	route {
		gateway_id = var.ig_id
		cidr_block = "0.0.0.0/0"
	}

    tags = {
        Name = "Public Subnet Route Table"
    }
}

resource "aws_route_table_association" "public" {
	subnet_id      = var.public_subnet_id
	route_table_id = aws_route_table.public.id
}