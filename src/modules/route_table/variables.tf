variable "vpc_id" {
    description = "ID of the VPC to create the Internet Gateway in"
    type = string
}

variable "ig_id" {
    description = "ID of the VPC's Internet Gateway"
    type = string
}

variable "public_subnet_id" {
    description = "ID of the VPC's public subnet"
    type = string
}