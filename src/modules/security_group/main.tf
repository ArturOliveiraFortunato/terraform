resource "aws_security_group" "public" {
  name        = "webserver"
  description = "Allows SSH connections, http requests (port 80) and ICMP messages from anywhere"
  vpc_id      = var.vpc_id

    ingress {
        description      = "HTTP from anywhere"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    ingress {
        description      = "ICMP"
        from_port        = -1
        to_port          = -1
        protocol         = "icmp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    ingress {
        description      = "SSH"
        from_port        = 22
        to_port          = 22
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
        description      = "Allow all outbound traffic"
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
        Name = "webserver"
    }
}

resource "aws_security_group" "private" {
    name        = "database"
    description = "Allows SSH connections and TCP requests (port 3110) from the public subnet, and ICMP messages from anywhere"
    vpc_id      = var.vpc_id

    ingress {
        description      = "TCP for webserver communication"
        from_port        = 3110
        to_port          = 3110
        protocol         = "tcp"
        cidr_blocks      = [var.public_subnet_cidr]
    }

    ingress {
        description      = "ICMP"
        from_port        = -1
        to_port          = -1
        protocol         = "icmp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    ingress {
        description      = "SSH from public subnet"
        from_port        = 22
        to_port          = 22
        protocol         = "tcp"
        cidr_blocks      = [var.public_subnet_cidr]
    }

    tags = {
        Name = "database"
    }
}