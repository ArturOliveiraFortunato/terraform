resource "aws_instance" "name" {
    for_each = var.configuration

    ami = each.value.ami
    instance_type = each.value.instance_type
    key_name = each.value.key_name

    subnet_id = each.value.subnet_id
    vpc_security_group_ids = each.value.security_groups
    associate_public_ip_address = each.value.associate_public_ip_address

    tags = each.value.tags
}