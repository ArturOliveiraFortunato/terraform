variable "configuration" {
    description = "EC2 instances' configuration. One for each instance to create"
    type = map(object({
        instance_type = string
        ami = string
        subnet_id = string
        associate_public_ip_address = bool
        key_name = string
        security_groups = list(string)
        tags = map(string)
    }))
    default = {}
}