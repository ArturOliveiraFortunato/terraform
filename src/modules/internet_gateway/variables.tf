variable "vpc_id" {
    description = "ID of the VPC to create the Internet Gateway in"
    type = string
}